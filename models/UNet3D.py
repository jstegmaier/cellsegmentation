# -*- coding: utf-8 -*-

import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision
import pytorch_lightning as pl

from argparse import ArgumentParser
from collections import OrderedDict
from torch.utils.data import DataLoader
from dataloader.h5_dataloader import MeristemH5Dataset
from utils.radam import RAdam



class UNet3D_module(nn.Module):
    """Implementation of the 3D U-Net architecture.
    """

    def __init__(self, patch_size, in_channels, out_channels, feat_channels=16, out_activation='sigmoid'):
        super(UNet3D_module, self).__init__()
        
        self.patch_size = patch_size
        self.in_channels = in_channels
        self.out_channels = out_channels
        self.feat_channels = feat_channels
        self.out_activation = out_activation # relu | sigmoid | tanh | hardtanh | none
        
        # Define layer instances       
        self.c1 = nn.Sequential(
            nn.Conv3d(in_channels, feat_channels//2, kernel_size=3, padding=1),
            nn.InstanceNorm3d(feat_channels//2),
            nn.LeakyReLU(negative_slope=0.2, inplace=True),
            nn.Conv3d(feat_channels//2, feat_channels, kernel_size=3, padding=1),
            nn.InstanceNorm3d(feat_channels),
            nn.LeakyReLU(negative_slope=0.2, inplace=True)
            )
        self.d1 = nn.Sequential(
            nn.Conv3d(feat_channels, feat_channels, kernel_size=4, stride=2, padding=1),
            nn.InstanceNorm3d(feat_channels),
            nn.LeakyReLU(negative_slope=0.2, inplace=True)
            )

        self.c2 = nn.Sequential(
            nn.Conv3d(feat_channels, feat_channels, kernel_size=3, padding=1),
            nn.InstanceNorm3d(feat_channels),
            nn.LeakyReLU(negative_slope=0.2, inplace=True),
            nn.Conv3d(feat_channels, feat_channels*2, kernel_size=3, padding=1),
            nn.InstanceNorm3d(feat_channels*2),
            nn.LeakyReLU(negative_slope=0.2, inplace=True)
            )
        self.d2 = nn.Sequential(
            nn.Conv3d(feat_channels*2, feat_channels*2, kernel_size=4, stride=2, padding=1),
            nn.InstanceNorm3d(feat_channels*2),
            nn.LeakyReLU(negative_slope=0.2, inplace=True)
            )

        self.c3 = nn.Sequential(
            nn.Conv3d(feat_channels*2, feat_channels*2, kernel_size=3, padding=1),
            nn.InstanceNorm3d(feat_channels*2),
            nn.LeakyReLU(negative_slope=0.2, inplace=True),
            nn.Conv3d(feat_channels*2, feat_channels*4, kernel_size=3, padding=1),
            nn.InstanceNorm3d(feat_channels*4),
            nn.LeakyReLU(negative_slope=0.2, inplace=True)
            )
        self.d3 = nn.Sequential(
            nn.Conv3d(feat_channels*4, feat_channels*4, kernel_size=4, stride=2, padding=1),
            nn.InstanceNorm3d(feat_channels*4),
            nn.LeakyReLU(negative_slope=0.2, inplace=True)
            )

        self.c4 = nn.Sequential(
            nn.Conv3d(feat_channels*4, feat_channels*4, kernel_size=3, padding=1),
            nn.InstanceNorm3d(feat_channels*4),
            nn.LeakyReLU(negative_slope=0.2, inplace=True),
            nn.Conv3d(feat_channels*4, feat_channels*8, kernel_size=3, padding=1),
            nn.InstanceNorm3d(feat_channels*8),
            nn.LeakyReLU(negative_slope=0.2, inplace=True)
            )
        self.u1 = nn.Sequential(
            nn.ConvTranspose3d(feat_channels*8, feat_channels*8, kernel_size=3, stride=2, padding=1, output_padding=1),
            nn.InstanceNorm3d(feat_channels*8),
            nn.LeakyReLU(negative_slope=0.2, inplace=True),
            nn.Conv3d(feat_channels*8, feat_channels*8, kernel_size=1),
            nn.InstanceNorm3d(feat_channels*8),
            nn.LeakyReLU(negative_slope=0.2, inplace=True)
            )
        self.c5 = nn.Sequential(
            nn.Conv3d(feat_channels*12, feat_channels*4, kernel_size=3, padding=1),
            nn.InstanceNorm3d(feat_channels*4),
            nn.LeakyReLU(negative_slope=0.2, inplace=True),
            nn.Conv3d(feat_channels*4, feat_channels*4, kernel_size=3, padding=1),
            nn.InstanceNorm3d(feat_channels*4),
            nn.LeakyReLU(negative_slope=0.2, inplace=True)
            )

        self.u2 = nn.Sequential(
            nn.ConvTranspose3d(feat_channels*4, feat_channels*4, kernel_size=3, stride=2, padding=1, output_padding=1),
            nn.InstanceNorm3d(feat_channels*4),
            nn.LeakyReLU(negative_slope=0.2, inplace=True),
            nn.Conv3d(feat_channels*4, feat_channels*4, kernel_size=1),
            nn.InstanceNorm3d(feat_channels*4),
            nn.LeakyReLU(negative_slope=0.2, inplace=True)
            )
        self.c6 = nn.Sequential(
            nn.Conv3d(feat_channels*6, feat_channels*2, kernel_size=3, padding=1),
            nn.InstanceNorm3d(feat_channels*2),
            nn.LeakyReLU(negative_slope=0.2, inplace=True),
            nn.Conv3d(feat_channels*2, feat_channels*2, kernel_size=3, padding=1),
            nn.InstanceNorm3d(feat_channels*2),
            nn.LeakyReLU(negative_slope=0.2, inplace=True)
            )

        self.u3 = nn.Sequential(
            nn.ConvTranspose3d(feat_channels*2, feat_channels*2, kernel_size=3, stride=2, padding=1, output_padding=1),
            nn.InstanceNorm3d(feat_channels*2),
            nn.LeakyReLU(negative_slope=0.2, inplace=True),
            nn.Conv3d(feat_channels*2, feat_channels*2, kernel_size=1),
            nn.InstanceNorm3d(feat_channels*2),
            nn.LeakyReLU(negative_slope=0.2, inplace=True)
            )
        self.c7 = nn.Sequential(
            nn.Conv3d(feat_channels*3, feat_channels, kernel_size=3, padding=1),
            nn.InstanceNorm3d(feat_channels),
            nn.LeakyReLU(negative_slope=0.2, inplace=True),
            nn.Conv3d(feat_channels, feat_channels, kernel_size=3, padding=1),
            nn.InstanceNorm3d(feat_channels),
            nn.LeakyReLU(negative_slope=0.2, inplace=True)
            )
        
        self.out = nn.Sequential(
            nn.Conv3d(feat_channels, feat_channels, kernel_size=3, padding=1),
            nn.InstanceNorm3d(feat_channels),
            nn.LeakyReLU(negative_slope=0.2, inplace=True),
            nn.Conv3d(feat_channels, out_channels, kernel_size=1)
            )
       
        if self.out_activation == 'relu':
            self.out_fcn = nn.ReLU()
        elif self.out_activation == 'sigmoid':
            self.out_fcn = nn.Sigmoid()
        elif self.out_activation == 'tanh':
            self.out_fcn = nn.Tanh()
        elif self.out_activation == 'hardtanh':
            self.out_fcn = nn.Hardtanh(0, 1)
        elif self.out_activation == 'none':
            self.out_fcn = None
        else:
            raise ValueError('Unknown output activation "{0}". Choose from "relu|sigmoid|tanh|hardtanh|none".'.format(self.out_activation))
        

    def forward(self, img):
        
        c1 = self.c1(img)
        d1 = self.d1(c1)
        
        c2 = self.c2(d1)
        d2 = self.d2(c2)
        
        c3 = self.c3(d2)
        d3 = self.d3(c3)
        
        c4 = self.c4(d3)
        
        u1 = self.u1(c4)
        c5 = self.c5(torch.cat((u1,c3),1))
        
        u2 = self.u2(c5)
        c6 = self.c6(torch.cat((u2,c2),1))
        
        u3 = self.u3(c6)
        c7 = self.c7(torch.cat((u3,c1),1))
        
        out = self.out(c7)
        if not self.out_fcn is None:
            out = self.out_fcn(out)
        
        return out
        
    
    
    
class UNet3D(pl.LightningModule):
    
    def __init__(self, hparams):
        super(UNet3D, self).__init__()
        self.hparams = hparams

        # networks
        self.network = UNet3D_module(patch_size=hparams.patch_size, in_channels=hparams.in_channels, out_channels=hparams.out_channels, out_activation=hparams.out_activation)

        # cache for generated images
        self.last_predictions = None
        self.last_imgs = None
        self.last_masks = None


    def forward(self, z):
        return self.network(z)


    def background_loss(self, y_hat, y):
        return F.binary_cross_entropy(y_hat, y)


    def boundary_loss(self, y_hat, y):
        return F.binary_cross_entropy(y_hat, y)
    
    
    def seed_loss(self, y_hat, y):
        loss = F.mse_loss(y_hat, y, reduction='none')
        weight = torch.clamp(y, min=0.1, max=1.0)
        loss = torch.mul(loss, weight)        
        return torch.mean(loss)


    def training_step(self, batch, batch_idx):
        
        # Get image ans mask of current batch
        self.last_imgs, self.last_masks = batch['image'], batch['mask']
        
        # generate images
        self.predictions = self.forward(self.last_imgs)
                
        # get the losses
        loss_bg = self.hparams.background_weight * self.background_loss(self.predictions[:,0,...], self.last_masks[:,0,...])
        loss_seed = self.hparams.seed_weight * self.seed_loss(self.predictions[:,1,...], self.last_masks[:,1,...])
        loss_boundary = self.hparams.boundary_weight * self.boundary_loss(self.predictions[:,2,...], self.last_masks[:,2,...])
        
        loss = loss_bg + loss_seed + loss_boundary
        tqdm_dict = {'bg_loss': loss_bg, 'seed_loss':loss_seed, 'boundary_loss': loss_boundary}
        output = OrderedDict({
            'loss': loss,
            'progress_bar': tqdm_dict,
            'log': tqdm_dict
        })
        return output
    
        
    def test_step(self, batch, batch_idx):
        x, y = batch['image'], batch['mask']
        y_hat = self.forward(x)
        return {'test_loss': F.l1_loss(y_hat, y)} 

    def test_end(self, outputs):
        avg_loss = torch.stack([x['test_loss'] for x in outputs]).mean()
        tensorboard_logs = {'test_loss': avg_loss}
        return {'avg_test_loss': avg_loss, 'log': tensorboard_logs}
    
    def validation_step(self, batch, batch_idx):
        x, y = batch['image'], batch['mask']
        y_hat = self.forward(x)
        return {'val_loss': F.mse_loss(y_hat, y)} 

    def validation_end(self, outputs):
        avg_loss = torch.stack([x['val_loss'] for x in outputs]).mean()
        tensorboard_logs = {'val_loss': avg_loss}
        return {'avg_val_loss': avg_loss, 'log': tensorboard_logs}

    def configure_optimizers(self):
        opt = RAdam(self.network.parameters(), lr=self.hparams.learning_rate)
        return [opt], []

    @pl.data_loader
    def train_dataloader(self):
         if self.hparams.train_list is None:
            return None
         else:
            dataset = MeristemH5Dataset(self.hparams.train_list, self.hparams.data_root, patch_size=self.hparams.patch_size,\
                                        image_group=self.hparams.image_group, mask_groups=self.hparams.mask_groups, dist_handling='bool', seed_handling='float')
            return DataLoader(dataset, batch_size=self.hparams.batch_size, shuffle=True, drop_last=True)
    
    @pl.data_loader
    def test_dataloader(self):
        if self.hparams.test_list is None:
            return None
        else:
            dataset = MeristemH5Dataset(self.hparams.test_list, self.hparams.data_root, patch_size=self.hparams.patch_size,\
                                        image_group=self.hparams.image_group, mask_groups=self.hparams.mask_groups, dist_handling='bool', seed_handling='float')
            return DataLoader(dataset, batch_size=self.hparams.batch_size)
    
    @pl.data_loader
    def val_dataloader(self):
        if self.hparams.val_list is None:
            return None
        else:
            dataset = MeristemH5Dataset(self.hparams.val_list, self.hparams.data_root, patch_size=self.hparams.patch_size,\
                                        image_group=self.hparams.image_group, mask_groups=self.hparams.mask_groups, dist_handling='bool', seed_handling='float')
            return DataLoader(dataset, batch_size=self.hparams.batch_size)


    def on_epoch_end(self):
        
        # log sampled images
        predictions = self.forward(self.last_imgs)
        prediction_grid = torchvision.utils.make_grid(predictions[:,:,20,:,:])
        self.logger.experiment.add_image('generated_images', prediction_grid, self.current_epoch)
        
        img_grid = torchvision.utils.make_grid(self.last_imgs[:,:,20,:,:])
        self.logger.experiment.add_image('raw_images', img_grid, self.current_epoch)
        
        mask_grid = torchvision.utils.make_grid(self.last_masks[:,:,20,:,:])
        self.logger.experiment.add_image('input_masks', mask_grid, self.current_epoch)
        
        
    @staticmethod
    def add_model_specific_args(parent_parser): 
        """
        Parameters you define here will be available to your model through self.hparams
        """
        parser = ArgumentParser(parents=[parent_parser])

        # network params
        parser.add_argument('--in_channels', default=1, type=int)
        parser.add_argument('--out_channels', default=3, type=int)
        parser.add_argument('--patch_size', default=(64,128,128), type=int, nargs='+')

        # data
        parser.add_argument('--data_root', default='data/PNAS', type=str) 
        parser.add_argument('--train_list', default='data/filelists/PNAS_h5_train.csv', type=str)
        parser.add_argument('--test_list', default='data/filelists/PNAS_h5_test.csv', type=str)
        parser.add_argument('--val_list', default='data/filelists/PNAS_h5_val.csv', type=str)
        parser.add_argument('--image_group', default='data/image', type=str)
        parser.add_argument('--mask_groups', default=('data/distance', 'data/seeds', 'data/boundary'), type=str, nargs='+')

        # training params (opt)
        parser.add_argument('--batch_size', default=1, type=int)
        parser.add_argument('--learning_rate', default=0.001, type=float)
        parser.add_argument('--background_weight', default=1, type=float)
        parser.add_argument('--seed_weight', default=20, type=float)
        parser.add_argument('--boundary_weight', default=1, type=float)
        parser.add_argument('--out_activation', default='sigmoid', type=str)
        
        return parser