# -*- coding: utf-8 -*-


import os
import h5py
import csv
import itertools
import numpy as np

from scipy.ndimage import filters
from torch.utils.data import Dataset




class MeristemH5Dataset(Dataset):
    """
    Dataset of fluorescently labeled cell membranes
    """
    
    def __init__(self, list_path, data_dir, patch_size=(64,128,128), norm_method='minmax', shuffle=True,\
                 image_group='data/image', mask_groups=('data/distance', 'data/seeds', 'data/boundary'), \
                 dist_handling='bool', seed_handling='float', correspondence=True, no_mask=False):
        
        
        # Check if the patch size is valid
        assert len(patch_size)==3, 'Patch size must be 3-dimensional.'
        
        # Save parameters
        self.data_dir = data_dir
        self.list_path = list_path 
        self.patch_size = patch_size
        self.norm_method = norm_method
        self.dist_handling = dist_handling
        self.seed_handling = seed_handling
        self.correspondence = correspondence
        self.no_mask = no_mask
        
        # Read the filelist and construct full paths to each file
        self.shuffle = shuffle
        self.image_group = image_group
        self.mask_groups = mask_groups
        self.data_list = self._read_list()
        
        # Get image statistics from up to 10 files
        print('Getting statistics from images...')
        self.data_statictics = {'min':[], 'max':[], 'mean':[], 'std':[]}
        for file_pair in self.data_list[:10]:
            with h5py.File(file_pair[0], 'r') as f_handle:
                image = f_handle[self.image_group] 
                self.data_statictics['min'].append(np.min(image))
                self.data_statictics['max'].append(np.max(image))
                self.data_statictics['mean'].append(np.mean(image))
                self.data_statictics['std'].append(np.std(image))
        
        # Average statistics
        self.data_statictics['min'] = np.min(self.data_statictics['min'])
        self.data_statictics['max'] = np.max(self.data_statictics['max'])
        self.data_statictics['mean'] = np.mean(self.data_statictics['mean'])
        self.data_statictics['std'] = np.mean(self.data_statictics['std'])
        
        if self.norm_method == 'minmax':
            self.norm1 = self.data_statictics['min']
            self.norm2 = self.data_statictics['max']-self.data_statictics['min']
        elif self.norm_method == 'meanstd':
            self.norm1 = self.data_statictics['mean']
            self.norm2 = self.data_statictics['std']
        else:
            self.norm1 = 0
            self.norm2 = 1
        
        
    def _read_list(self):
        
        # Read the filelist and create full paths to each file
        filelist = []    
        with open(self.list_path, 'r') as f:
            reader = csv.reader(f, delimiter=';')
            for row in reader:
                if len(row)==0: continue
                row = [os.path.join(self.data_dir, r) for r in row]
                filelist.append(row)
        
        if self.shuffle:
            np.random.shuffle(filelist)
                
        return filelist
    
    
    def __len__(self):
        
        return len(self.data_list)
    
    
    def __getitem__(self, idx):
        
        # Get the paths to the image and mask
        filepath = self.data_list[idx]
        
        # Load the image patch
        with h5py.File(filepath[0], 'r') as f_handle:
            image = f_handle[self.image_group]   
                        
            # Determine the patch position
            rnd_start = [np.random.randint(0, np.maximum(1,image_dim-patch_dim)) for patch_dim, image_dim in zip(self.patch_size, image.shape)]
            rnd_end = [start+patch_dim for start, patch_dim in zip(rnd_start, self.patch_size)]    
            slicing = tuple(map(slice, rnd_start, rnd_end))            
            image = image[slicing]
            
            # Pad if neccessary
            pad_width = [(0,np.maximum(0,p-i)) for p,i in zip(self.patch_size,image.shape)] 
            image = np.pad(image, pad_width, mode='reflect')
            
            image = image[np.newaxis,...]
            image = image.astype(np.float32)
            image -= self.norm1
            image /= self.norm2
            
            if self.norm_method == 'minmax':
                image = np.clip(image, 1e-5, 1)
            
        sample = {'image':image}
            
        
        if not self.no_mask:
            
            # Load the corresponding mask patch
            mask = np.zeros((len(self.mask_groups),)+self.patch_size, dtype=np.float32)
            with h5py.File(filepath[1], 'r') as f_handle:
                for num_group, group_name in enumerate(self.mask_groups):
                    
                    mask_tmp = f_handle[group_name]                    
                    if not self.correspondence and num_group==0:
                        rnd_start = [np.random.randint(0, np.maximum(1,mask_dim-patch_dim)) for patch_dim, mask_dim in zip(self.patch_size, mask_tmp.shape)]
                        rnd_end = [start+patch_dim for start, patch_dim in zip(rnd_start, self.patch_size)]    
                        slicing = tuple(map(slice, rnd_start, rnd_end))                        
                    mask_tmp = mask_tmp[slicing]
                    
                    # Pad if neccessary
                    pad_width = [(0,np.maximum(0,p-i)) for p,i in zip(self.patch_size,mask_tmp.shape)] 
                    mask_tmp = np.pad(mask_tmp, pad_width, mode='reflect')
                    
                    mask[num_group,...] = mask_tmp
                    if 'distance' in group_name:
                        if self.dist_handling == 'float':
                            mask[num_group,...] /= 100
                        elif self.dist_handling == 'bool':
                            mask[num_group,...] = mask[num_group,...]<0
                        else:
                            pass
                    if 'seed' in group_name:                    
                        if self.seed_handling == 'float':
                            mask[num_group,...] = filters.gaussian_filter(mask[num_group,...], 2)
                            mask[num_group,...] /= mask[num_group,...].max() if mask[num_group,...].max()>1e-4 else 1
                        elif self.seed_handling == 'bool':
                            mask[num_group,...] = mask[num_group,...]>0.1
                        else:
                            pass
                        
            mask = mask.astype(np.float32)
            sample['mask'] = mask
            
        return sample
    

    
    
    
    
class MeristemH5Tiler(Dataset):
    
    """
    Dataset of fluorescently labeled cell membranes
    """
    
    def __init__(self, list_path, data_dir, patch_size=(64,128,128), overlap=0, norm_method='minmax',\
                 image_group='data/image', mask_groups=('data/distance', 'data/seeds', 'data/boundary'), \
                 dist_handling='bool', seed_handling='float'):
        """
        Args:
            list_path (str): Path to the csv file containing all hdf5 filenames of image-mask-pairs
            data_dir (str): Path to the folder containing all data
            patch_size (tuple): Size of each loaded patch
            overlap (int): Number of pixels overlapping between adjacent patches
            norm_method (str): Either 'None', 'minmax' or 'meanstd'
        """        
        
        # Check if the patch size is valid
        assert len(patch_size)==3, 'Patch size must be 3-dimensional.'
        
        # Save parameters
        self.data_dir = data_dir
        self.list_path = os.path.abspath(list_path) 
        self.patch_size = patch_size
        self.overlap = overlap
        self.norm_method = norm_method
        self.dist_handling = dist_handling
        self.seed_handling = seed_handling
        
        # Read the filelist and construct full paths to each file
        self.image_group = image_group
        self.mask_groups = mask_groups
        self.data_list = self._read_list()
        self.set_data_idx(0)
        
        # Get image statistics from up to 10 files
        print('Getting statistics from images...')
        self.data_statictics = {'min':[], 'max':[], 'mean':[], 'std':[]}
        for file_pair in self.data_list[:10]:
            with h5py.File(file_pair[0], 'r') as f_handle:
                image = f_handle[self.image_group] 
                self.data_statictics['min'].append(np.min(image))
                self.data_statictics['max'].append(np.max(image))
                self.data_statictics['mean'].append(np.mean(image))
                self.data_statictics['std'].append(np.std(image))
        
        # Average statistics
        self.data_statictics['min'] = np.min(self.data_statictics['min'])
        self.data_statictics['max'] = np.max(self.data_statictics['max'])
        self.data_statictics['mean'] = np.mean(self.data_statictics['mean'])
        self.data_statictics['std'] = np.mean(self.data_statictics['std'])
        
        if self.norm_method == 'minmax':
            self.norm1 = self.data_statictics['min']
            self.norm2 = self.data_statictics['max']-self.data_statictics['min']
        elif self.norm_method == 'meanstd':
            self.norm1 = self.data_statictics['mean']
            self.norm2 = self.data_statictics['std']
        else:
            self.norm1 = 0
            self.norm2 = 1
        
        
    def _read_list(self):
        
        # Read the filelist and create full paths to each file
        filelist = []    
        with open(self.list_path, 'r') as f:
            reader = csv.reader(f, delimiter=';')
            for row in reader:
                row = [os.path.abspath(os.path.join(self.data_dir, r)) for r in row]
                filelist.append(row)
                
        return filelist
    
    
    def set_data_idx(self, idx):
        
        # Restrict the idx to the maximum data idx
        idx = idx%len(self.data_list)
        self.data_idx = idx
        
        # Get the current data size
        with h5py.File(self.data_list[idx][0], 'r') as f_handle:
            image = f_handle[self.image_group]
            self.data_shape = image.shape[:3]
        
        # Calculate the number and positions of tiles
        locations = []
        for i,p in zip(self.data_shape, self.patch_size):
            # get left coords
            coords = np.arange(np.ceil(i/(p-self.overlap)), dtype=np.uint16)*(p-self.overlap)
            # ensure to not get out of bounds at the rigth image boundary
            coords = np.minimum(coords, np.maximum(0,i-p))
            locations.append(coords)
        self.locations = list(itertools.product(*locations))
    
    
    def __len__(self):
        
        return len(self.locations)
    
    
    def __getitem__(self, idx):
        
        self.patch_start = self.locations[idx]
        self.patch_end = [start+patch_dim for start,patch_dim in zip(self.patch_start, self.patch_size)]  
        slicing = tuple(map(slice, self.patch_start, self.patch_end))
                
        # Load the image patch
        with h5py.File(self.data_list[self.data_idx][0], 'r') as f_handle:
            image = f_handle[self.image_group] 
            image = image[slicing]
            
            # Pad if neccessary
            pad_width = [(0,np.maximum(0,p-i)) for p,i in zip(self.patch_size,image.shape)] 
            image = np.pad(image, pad_width, mode='reflect')
            
            # Normalize the image
            image = image[np.newaxis,...]
            image = image.astype(np.float32)
            image -= self.norm1
            image /= self.norm2
            
        sample = {'image':image}
            
        # Load the corresponding mask patch
        if not self.mask_groups is None:
            mask = np.zeros((len(self.mask_groups),)+self.patch_size, dtype=np.float32)
            with h5py.File(self.data_list[self.data_idx][1], 'r') as f_handle:
                for num_group, group_name in enumerate(self.mask_groups):
                    mask_tmp = f_handle[group_name]
                    mask_tmp = mask_tmp[slicing]
                    
                    # Pad if neccessary
                    pad_width = [(0,np.maximum(0,p-i)) for p,i in zip(self.patch_size,mask_tmp.shape)] 
                    mask_tmp = np.pad(mask_tmp, pad_width, mode='reflect')
                    
                    mask[num_group,...] = mask_tmp
                    if 'distance' in group_name:
                        if self.dist_handling == 'float':
                            mask[num_group,...] /= 100
                        elif self.dist_handling == 'bool':
                            mask[num_group,...] = mask[num_group,...]<0
                        else:
                            pass
                    if 'seed' in group_name:                    
                        if self.seed_handling == 'float':
                            mask[num_group,...] = filters.gaussian_filter(mask[num_group,...], 4)
                            mask[num_group,...] /= mask[num_group,...].max() if mask[num_group,...].max()>1e-4 else 1
                        elif self.seed_handling == 'bool':
                            mask[num_group,...] = mask[num_group,...]>0.1
                        else:
                            pass
                mask = mask.astype(np.float32)
                
            sample['mask'] = mask
        
        return sample
            
        