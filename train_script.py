# -*- coding: utf-8 -*-


from argparse import ArgumentParser

import numpy as np
import torch
import os

from models.UNet3D import UNet3D

from pytorch_lightning import Trainer
from pytorch_lightning.logging import TestTubeLogger
from pytorch_lightning.callbacks import ModelCheckpoint

SEED = 1
torch.manual_seed(SEED)
np.random.seed(SEED)


def main(hparams):
    
    
    """
    Main training routine specific for this project
    :param hparams:
    """

    # ------------------------
    # 1 INIT LIGHTNING MODEL
    # ------------------------
    model = UNet3D(hparams)

    # ------------------------
    # 2 INIT TRAINER
    # ------------------------
    os.makedirs(os.path.abspath(hparams.output_path), exist_ok=True)
    os.makedirs(os.path.abspath(hparams.log_path), exist_ok=True)
    
    checkpoint_callback = ModelCheckpoint(
        filepath=os.path.abspath(hparams.output_path),
        save_top_k=-1,
        verbose=True,
        monitor='val_loss',
        period=200
    )
    
    logger = TestTubeLogger(
        save_dir=os.path.abspath(hparams.log_path),
        name='lightning_logs'
    )
    
    trainer = Trainer(
        logger=logger,
        checkpoint_callback=checkpoint_callback,
        gpus=1,
        distributed_backend=hparams.distributed_backend,
        min_nb_epochs=hparams.epochs,
        max_nb_epochs=hparams.epochs,
        early_stop_callback=False
    )

    # ------------------------
    # 3 START TRAINING
    # ------------------------
    trainer.fit(model)
    trainer.test()


if __name__ == '__main__':
    # ------------------------
    # TRAINING ARGUMENTS
    # ------------------------
    # these are project-wide arguments

    parent_parser = ArgumentParser(add_help=False)

    # gpu args
    parent_parser.add_argument(
        '--output_path',
        type=str,
        default='results/UNet3D_pyTorch',
        help='output path for test results'
    )
    
    parent_parser.add_argument(
        '--log_path',
        type=str,
        default='logs/',
        help='output path for logs'
    )
    
    parent_parser.add_argument(
        '--distributed_backend',
        type=str,
        default='dp',
        help='supports three options dp, ddp, ddp2'
    )
    
    parent_parser.add_argument(
        '--epochs',
        type=int,
        default=1000,
        help='number of epochs'
    )
    
    # each LightningModule defines arguments relevant to it
    parser = UNet3D.add_model_specific_args(parent_parser)
    hyperparams = parser.parse_args()

    # ---------------------
    # RUN TRAINING
    # ---------------------
    main(hyperparams)
