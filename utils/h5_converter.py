# -*- coding: utf-8 -*-


import os
import h5py
import glob
import numpy as np

from skimage import io, morphology, measure
from scipy.ndimage import distance_transform_edt




def h5_writer(data_list, save_path, group_root='data', group_names=['data']):
    
    save_path = os.path.abspath(save_path)
    
    assert(len(data_list)==len(group_names)), 'Each data matrix needs a group name'
    
    with h5py.File(save_path, 'w') as f_handle:
        grp = f_handle.create_group(group_root)
        for data, group_name in zip(data_list, group_names):
            grp.create_dataset(group_name, data=data, chunks=True, compression='gzip')



def convert_instances(instance_mask):
    
    # get the membrane mask
    membrane_mask = morphology.dilation(instance_mask, selem=morphology.ball(3)) - instance_mask
    membrane_mask = membrane_mask != 0
    membrane_mask = membrane_mask.astype(np.float32)
    
    # get the distance mask
    distance_mask = distance_transform_edt(instance_mask>1)        
    distance_mask = distance_mask - distance_transform_edt(instance_mask<=1)
    distance_mask = distance_mask.astype(np.float32)
    
    # get the centroid mask
    centroid_mask = np.zeros(instance_mask.shape, dtype=np.float32)
    regions = measure.regionprops(instance_mask)
    for props in regions:
        c = props.centroid
        centroid_mask[np.int(c[0]), np.int(c[1]), np.int(c[2])] = 1
        
    return membrane_mask, distance_mask, centroid_mask



def prepare_PNAS(data_path='/images/PNAS'):
    
    data_path = os.path.abspath(data_path)
    
    plant_names = ['plant1', 'plant2', 'plant4', 'plant13', 'plant15', 'plant18']
    
    for plant in plant_names:
        
        
        # convert masks
        mask_list = glob.glob(os.path.join(data_path, plant, 'segmentation_tiffs', '*.tif'))
        for num_file,file in enumerate(mask_list):
            
            print('Processing mask {0}/{1} of {2}'.format(num_file+1, len(mask_list), plant))
            
            instance_mask = io.imread(file)
            
            # convert the instance mask
            membrane_mask, distance_mask, centroid_mask = convert_instances(instance_mask)
                
            # save the data
            save_name = os.path.split(file)[-1]
            save_name = os.path.join(data_path, plant, 'segmentation_tiffs', 'boundary_encoding_'+save_name[:-4]+'.h5')
            h5_writer([membrane_mask, distance_mask, centroid_mask], save_name, group_root='data', group_names=['boundary', 'distance', 'seeds'])
    
    
        # convert masks
        image_list = glob.glob(os.path.join(data_path, plant, 'processed_tiffs', '*YFP*.tif'))
        for num_file,file in enumerate(image_list):
            
            print('Processing image {0}/{1} of {2}'.format(num_file+1, len(image_list), plant))
            
            # Exclude the one file without segmentation
            if plant=='plant18' and '40hrs' in file:
                continue
            
            processed_img = io.imread(file)                
            
            # save the data
            save_name = os.path.split(file)[-1]
            save_name = os.path.join(data_path, plant, 'processed_tiffs', save_name[:-4]+'.h5')
            h5_writer([processed_img], save_name, group_root='data', group_names=['image'])
    
    
    
def prepare_images(data_path, group_root='data', group_name='image', downscale_factor=1, include_z=False):
    
    filelist = glob.glob(os.path.join(data_path, '*.tif'))
    
    for num_file,file in enumerate(filelist):
        
        print('Processing image {0}/{1}'.format(num_file+1, len(filelist)))
        
        img = io.imread(file)
        
        # remove possible artificial background values
        img[img==img.min()] = np.unique(img)[1]
        img[img==img.max()] = np.unique(img)[-2]
        
        # resize the image
        downscale_factor = int(downscale_factor)
        z_step = downscale_factor if include_z else 1
        img = img[::z_step, ::downscale_factor, ::downscale_factor]
        
        # save the data
        save_name = file[:-4]+'.h5'
        h5_writer([img], save_name, group_root=group_root, group_names=[group_name])