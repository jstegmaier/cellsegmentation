# -*- coding: utf-8 -*-


import os
import glob
import csv
import numpy as np


def get_files(folders, data_root='', descriptor='', filetype='tif'):
    
    filelist = []
    
    for folder in folders:
        files = glob.glob(os.path.join(data_root, folder, '*'+descriptor+'*.'+filetype))
        filelist.extend([os.path.join(folder, os.path.split(f)[-1]) for f in files])
        
    return filelist
        
        
        
def create_csv(data_list, save_path='list_folder/experiment_name', test_split=0.2, val_split=0.1, shuffle=False):
        
    save_path = os.path.abspath(save_path)
    
    # Get number of files for each split
    num_files = len(data_list)
    num_test_files = int(test_split*num_files)
    num_val_files = int((num_files-num_test_files)*val_split)
    
    # Get file indices for each split
    file_idx = np.arange(num_files)
    test_idx = sorted(np.random.choice(file_idx, size=num_test_files, replace=False))
    val_idx = sorted(np.random.choice(list(set(file_idx)-set(test_idx)), size=num_val_files, replace=False))
    train_idx = sorted(list(set(file_idx) - set(test_idx) - set(val_idx)))
    
    # Save csv files
    if len(test_idx)>0:
        with open(save_path+'_test.csv', 'w', newline='') as fh:
            writer = csv.writer(fh, delimiter=';')
            for idx in test_idx:
                writer.writerow(data_list[idx])
    
    if len(val_idx)>0:
        with open(save_path+'_val.csv', 'w', newline='') as fh:
            writer = csv.writer(fh, delimiter=';')
            for idx in val_idx:
                writer.writerow(data_list[idx])
        
    if len(train_idx)>0:  
        with open(save_path+'_train.csv', 'w', newline='') as fh:
            writer = csv.writer(fh, delimiter=';')
            for idx in train_idx:
                writer.writerow(data_list[idx])
                
            
            
            
def create_PNAS_lists(data_root='data/PNAS', save_path='data/filelists', list_name='PNAS_h5', test_split=0.2, val_split=0.1):
    
    data_root = os.path.abspath(data_root)
    save_path = os.path.abspath(save_path)
    os.makedirs(save_path, exist_ok=True)
    
    folders = ['plant1', 'plant2', 'plant4', 'plant13', 'plant15', 'plant18']
    image_folders = [os.path.join(f, 'processed_tiffs') for f in folders]
    mask_folders = [os.path.join(f, 'segmentation_tiffs') for f in folders]
    
    image_list = sorted(get_files(image_folders, data_root=data_root, descriptor='YFP', filetype='h5'))
    mask_list = sorted(get_files(mask_folders, data_root=data_root, descriptor='YFP', filetype='h5'))
        
    data_list = [[i,m] for i,m in zip(image_list, mask_list)]
    create_csv(data_list, save_path=os.path.join(save_path,list_name), test_split=test_split, val_split=val_split)
    