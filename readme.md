# Segmentation of 3D Microscopy Data

This repository contains pipelines and tools for the segmentation of 3D microscopy image data. 
All algorithms are tested on Ubuntu 18 and specifically tested with 3D confocal microscopy images of fluorescently labeled cell membranes of *A. thaliana*, 
published in [Repo](https://www.repository.cam.ac.uk/handle/1810/262530), [Paper](https://www.pnas.org/content/113/51/E8238).

The pipeline is described in "CNN-based Preprocessing to Optimize Watershed-based Cell Segmentation in 3D Confocal Microscopy Images" (https://arxiv.org/abs/1810.06933).
For the original implementation of the pipeline, that was used to create the reported results, please refer to the ISBI2019 branch. 

#### Setting up the PyTorch environment
- Set up an environment manager, e.g., [Miniconda](https://docs.conda.io/en/latest/miniconda.html).
- Set up the environment using the `environment.yml` file or set it up manually by installing the packages listed in the yml-files.
- Note that, if you are not able to use your GPU, computation times might be largely increased.

#### Preparing the data
- Download the data, available at the Repository linked above and unpack it into the data folder.
- The data structure should be `data/PNAS/plantx`.
- We are using h5-file formats and 3-class segmentation masks, which are created by runnig the `preparation_script.py`.

#### Training the network
- Since we also provide pretrained weights for the meristem data set, this step is optional!
- For training run the `train_script.py`.
- When using a different data structure you can adjust the command line arguments.

#### Predicting segmentation masks
- For prediction of segmentation masks run the `application_script.py`.


#### Process your own images
- Create h5-files by running `prepare_images.py --data_path path/to/your/image/folder --name name_of_your_experiment`.
- Run the network by `application_script.py --data_root path/to/your/image/folder --test_list data/filelists/name_of_your_experiment_test.csv`.
- If the model has trouble finding instances, you can adjust the threshold parameters between (0,1) by adding `--bg_thresh 0.x --seed_thresh 0.x --boundary_thresh 0.x` to the call of the application script.