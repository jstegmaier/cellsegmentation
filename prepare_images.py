# -*- coding: utf-8 -*-

import os

from argparse import ArgumentParser
from utils.h5_converter import prepare_images
from utils.csv_generator import get_files, create_csv


if __name__ == '__main__':
    # ------------------------
    #  ARGUMENTS
    # ------------------------

    parser = ArgumentParser(add_help=False)

    parser.add_argument(
        '--data_path',
        type=str,
        default='data/PNAS',
        help='path to data files'
    )
    parser.add_argument(
        '--name',
        type=str,
        default='your_files',
        help='name of the experiment'
    )
    params = parser.parse_args()

    # ---------------------
    # RUN PREPARATION
    # ---------------------
    prepare_images(params.data_path)
    
    # ---------------------
    # CREATE FILELIST
    # ---------------------
    filelist = get_files([''], data_root=params.data_path, filetype='h5')
    filelist = [[f,] for f in filelist]
    os.makedirs('data/filelists/', exist_ok=True)
    create_csv(filelist, save_path='data/filelists/myfiles', test_split=1, val_split=0)