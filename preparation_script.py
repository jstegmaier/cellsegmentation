# -*- coding: utf-8 -*-


from argparse import ArgumentParser
from utils.h5_converter import prepare_PNAS


if __name__ == '__main__':
    # ------------------------
    #  ARGUMENTS
    # ------------------------

    parser = ArgumentParser(add_help=False)

    parser.add_argument(
        '--data_path',
        type=str,
        default='data/PNAS',
        help='path to data files'
    )
    params = parser.parse_args()

    # ---------------------
    # RUN PREPARATION
    # ---------------------
    prepare_PNAS(params.data_path)
